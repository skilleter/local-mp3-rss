#!/usr/bin/env python2
# encoding: utf-8

import os
import sys
import glob
import ConfigParser

import genRSS

# Entry point

FEED_ROOT_DIR = '/var/www/html'

# Read the configuration file

config = ConfigParser.ConfigParser()
config.read('mp3-rss.ini')

if not os.path.isdir(FEED_ROOT_DIR):
    os.mkdir(FEED_ROOT_DIR)

for feed_title in config.sections():

    feed_name = config.get(feed_title, 'feedname')
    feed_glob = config.get(feed_title, 'glob')
    feed_paths = config.get(feed_title, 'path').split(',')

    # Get the list of MP3 files in the search path for the feed

    print('*' * 80)
    print('Creating or updating RSS feed for %s with name %s' %
          (feed_title, feed_name))

    media_files = []
    images = []

    for feed_path in feed_paths:
        media = glob.glob(os.path.join(feed_path, feed_glob, '*.mp3'))
        print('  Found %d files in in %s/%s' % (len(media), feed_path, feed_glob))
        media_files += media

        # Look for at least one JPEG file in the search path

        images += glob.glob(os.path.join(feed_path, feed_glob, '*.jp*g'))

    print('Found %d matching media files' % len(media_files))
    print('Found %d images' % len(images))

    if len(images):
        image = images[0]
    else:
        image = None

    # Create the media directory if it doesn't already exist

    feedpath = os.path.join(FEED_ROOT_DIR, feed_name)
    if not os.path.isdir(feedpath):
        os.mkdir(feedpath)

    # Get the list of symlinks already present in the media directory

    existing = glob.glob('%s/*' % feedpath)

    print('%d files already present in %s' % (len(existing), feedpath))

    # Get list of symlinks that are already present, remove any that point
    # to files that are not in the media list

    done = []
    updated = False

    for media in existing:
        destination = os.readlink(media)

        if destination in media_files:
            done.append(destination)
        else:
            print('Removing obsolete symlink %s->%s' % (media, destination))
            os.unlink(media)
            updated = True

    for media in media_files:
        if media not in done:

            media_comp = media.split('/')
            medianame = os.path.join(feedpath, '%s-%s' % (media_comp[-2], media_comp[-1]))

            if not os.path.exists(medianame):
                print('Creating symlink from %s to %s' % (media, medianame))
                os.symlink(media, medianame)
                updated = True

    if updated:
        print('Generating new XML file')
        genRSS.generate(feedpath,
                        os.path.join(FEED_ROOT_DIR, '%s.xml' % feed_name),
                        feed_title,
                        'Downloaded from BBC iPlayer', 'http://server',
                        relative_path=FEED_ROOT_DIR,
                        image=image)
    else:
        print('No changes to feed')

